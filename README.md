# TransLeaner
Browser extension that will read in the words on a webpage and translate a given portion of them into a given language. 
The language and proportion of words to be translated are up to user input.
At the moment written specifically for Chrome, it should theoretically also work in Firefox but a few local storage calls would have to be altered.

## Installation
Clone this repo and load the root directory to Chrome as an unpacked extension. 

## Usage
Switch on and go to a webpage!

## Known Issues
 - About 50% of pages I have tested this on (not many) have completely broken. The current input of words
doesn't correctly filter out HTML/CSS tags.