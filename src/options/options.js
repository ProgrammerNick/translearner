let url = "https://translation.googleapis.com/language/translate/v2/languages?target=en&model=base"
url += "&key=gcloud_api_key"; //no push key!
let languages = [];
let languageCodes = [];

let page = document.getElementById("languageDiv");
let languageSelect = document.getElementById("languageSelect");
let submitButton = document.getElementById("languageSubmit");

let proportionRange = document.getElementById("proportionRange");
let proportionValue = document.getElementById("proportionValue");
let proportionSubmit = document.getElementById("proportionSubmit");

let request = new XMLHttpRequest();
request.open("GET", url);
request.send();

submitButton.addEventListener('click', () => {
  chrome.storage.sync.set({selectedLanguage: languageSelect.options[languageSelect.selectedIndex].value}, () => {
    alert("Language updated to " + languageSelect.options[languageSelect.selectedIndex].label);
  });
});

proportionRange.oninput = () => {
  proportionValue.value = proportionRange.value;
}

proportionValue.oninput = () => {
  if (proportionValue.value > 100) {
    proportionValue.value = 100;
  }
  proportionRange.value = proportionValue.value;
}

proportionSubmit.addEventListener('click', () => {
  if (proportionValue.value > 100) {
    proportionValue.value = 100;
    proportionRange.value = 100;
  }
  chrome.storage.sync.set({proportion: proportionValue.value}, () => {
    alert("Proportion of words to be translated updated to " + proportionValue.value + "%");
  })
})

chrome.storage.sync.get("proportion", (proportionData) => {
  if(proportionData && proportionData.value != undefined) {
    proportionRange.value = proportionData.proportion;
    proportionValue.value = proportionData.proportion;
  } else {
    proportionRange.value = "25";
    proportionValue.value = "25";
  }
  
});

console.log("Sending languages request...");
request.onreadystatechange = event => {
  if(request.readyState == 4 && request.status == 200) {
    var parsedResult = JSON.parse(request.response);
    console.log(parsedResult);
    
    for(let index in parsedResult.data.languages) {
      languages.push(parsedResult.data.languages[index].name);
      languageCodes.push(parsedResult.data.languages[index].language);
    }

    for(let i = 0; i < languages.length; i++) {
      let option = new Option(languages[i], languageCodes[i]);
      languageSelect.add(option);
    }
  } else if (request.status == 200) {
    console.log("Server returned 200, awaiting full response...");
  } else {
    console.log("Request failed!");
    console.log(request);
  }
}