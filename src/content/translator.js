let translateRequest = "https://translation.googleapis.com/language/translate/v2?model=base&source=en"
translateRequest += "&key=gcloud-api-key" // don't push api key 

let elements = document.getElementsByTagName('*');
let chosenWords = [];
let translatedWords = [];

console.log("Doing that words thing");

chrome.storage.sync.get('proportion', (proportionData) => {
  console.log("Loaded proportion: " + proportionData.proportion);

  for (let i = 0; i < elements.length; i++) {
    let element = elements[i];
    for (let j = 0; j < element.childNodes.length; j++) {
      let node = element.childNodes[j];
      if (node.nodeType === 3) {
        text = node.nodeValue.replace(/\W/gi, " ");
        let words = text.split(" ");
        for (let n = 0; n < words.length; n++) {
          const random = Math.floor(Math.random() * 100);
          if (random < proportionData.proportion) {
            chosenWords.push(words[n]);
          }
        }
      }
    }
  }

  const filteredWords = chosenWords.filter(word => word.length > 2); 

  console.log("Selected words after filter:" + filteredWords.length);

  // Only send 100 words for now (i.e. proportion doesn't actually do anything)
  let finalWords = [];
  while (finalWords.length < 100) {
    finalWords.push(filteredWords[Math.floor(Math.random() * filteredWords.length)]);
  }

  let params = "";

  for (let index = 0; index < finalWords.length; index++) {
    if(finalWords[index] && finalWords[index] != "" && finalWords[index] != " " && finalWords[index] != "\n") {
      params += "&q=" + finalWords[index];
    }
  }

  chrome.storage.sync.get('selectedLanguage', (selectedLanguageData) => {
    const request = new XMLHttpRequest();

    translateRequest += "&target=" + selectedLanguageData.selectedLanguage;

    const url = translateRequest += params;
    console.log("URL: " + url);
    request.open("POST", url);
    request.send();
    
    request.onreadystatechange = event => {
      if (request.readyState == 4 && request.status == 200) {
        var parsedResult = JSON.parse(request.response);

        for (let index in parsedResult.data.translations) {
          translatedWords.push(parsedResult.data.translations[index].translatedText);
        }

        console.log(translatedWords);

        for (let i = 0; i < elements.length; i++) {
          let element = elements[i];
          for (let j = 0; j < element.childNodes.length; j++) {
            let node = element.childNodes[j];
            if (node.nodeType === 3) {
              var text = node.nodeValue;
              var replacedText = text;
              for(let k = 0; k < finalWords.length; k++) {
                replacedText = replacedText.replace(finalWords[k], translatedWords[k]);
              }
              if (replacedText !== text) {
                element.replaceChild(document.createTextNode(replacedText), node);
              }
            }
          }
        }
      } else if (request.status == 200) {
        console.log("Server returned 200, awaiting full response...");
      } else {
        console.log("Request failed!");
        console.log(request);
      }
    }
  });
});